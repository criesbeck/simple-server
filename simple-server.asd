;;;; simple-server.asd

(asdf:defsystem #:simple-server
  :serial t
  :depends-on ((:feature :allegro (:require "aserve"))
               (:feature :lispworks #:hunchentoot)
               (:feature :sbcl #:hunchentoot))
  :components ((:file "package")
               (:file "aserve-server" :if-feature :allegro)
               (:file "hunchentoot-server" :if-feature (:or :lispworks :sbcl))
               (:file "simple-server")))
