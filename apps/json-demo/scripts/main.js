require(['require-config'], function() {

  require(["jquery"], function($) {
    
    // json save data

    $("#save-data-btn").on("click", function() {
      var key = $("#var").val();
      var val = $("#val").val();
      if (key) {
        var data = {};
        data[key] = val;

        $.ajax("/save-data", { 
          data: JSON.stringify(data), 
          dataType: "json", 
          type: "POST"
        }).done(function (response) {
          $("#saved-data").text(JSON.stringify(response));
        });
      }
    });
    
    // exported symbol search

    $("#search-btn").on("click", function() {
      var name = $("#name").val();
      var package = $("#package").val();
      if (name) {
        var data = {};
        data.name = name;
        if (package) data.package = package;

        $.getJSON("/exports", data).done(function(response) {
          var symbols = response.symbols || [];
          $("#symbols").text(symbols.join(", "));
        });
      }
    });

  });
  
});