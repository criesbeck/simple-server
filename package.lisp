(defpackage #:simple-server
  (:use #:common-lisp)
  (:export 
   ;; defined for webapps to use
   #:get-route-map #:param-value #:stop-server
   
   ;; defined for server implementations to use
   #:get-response-function #:set-response-function #:set-server
   #:*port-servers* #:*root-dir*
      
   ;; must be defined by server implementations
   #:defroute #:start-server
   ))
